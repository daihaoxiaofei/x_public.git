<?php

namespace Tool;

use App\Http\Model\Finance;
use App\Http\Model\Order;
use App\Http\Model\Team;
use App\Http\Model\User;
use App\Http\Model\User_invite;

class Check
{
	//检查用户购买数据记录:uh_t
	public static function check_u_t($do = 0)
	{
		$user_orders = Order::selectraw('sum(have_t) as uh_t, sum(`price`) as u_price, user_id')
			->where('is_go', 1)
			->groupBy('user_id')
			->get();
		return User_invite::select('user_id', 'uh_t', 'u_price')
			->get()
			->map(function ($u_i) use ($do, $user_orders) {
				$is_check = 0;
				$u_order = $user_orders->where('user_id', $u_i->user_id)->first();
				if ($u_order) {
					$uh_t = $u_order->uh_t;
					$u_price = $u_order->u_price;
				} else {
					$uh_t = 0;
					$u_price = 0;
				}

				$msg = [];
				if ($u_i->uh_t <> $uh_t) {
					$msg[] = [
						$u_i->user_id . ' 购买数错误 ' => [
							'原 ' => $u_i->uh_t,
							'应 ' => $uh_t,
						],
					];
					if ($do) {
						$u_i->uh_t = $uh_t;
						$is_check = 1;
						$msg[] = '已修复 购买数错误';
					}
				}
				if ($u_i->u_price <> $u_price) {
					$msg[] = [
						$u_i->user_id . ' 购买金额错误 ' => [
							'原 ' => $u_i->u_price,
							'应 ' => $u_price,
						],
					];
					if ($do) {
						$u_i->u_price = $u_price;
						$is_check = 1;
						$msg[] = '已修复 购买金额错误';
					}
				}
				if ($is_check) {
					$u_i->save();
				}
				return $msg;
			})
			->filter()// 去掉无效数据
			->values(); //  重新排序
	}

	//检查用户 团队记录:zh_t ,allh_t
	public static function check_team_t($do = 0)
	{
		$msg = [];
		// 所有有团队记录的用户id
		$user_ids = User_invite::groupBy('user_id')->pluck('user_id');
		User::whereIn('id', $user_ids)
			->get()
			->map(function ($user) use ($do, &$msg) {
				//获取旗下所有的用户id
				$invites_id = Team::whereRaw('FIND_IN_SET(?,teams)', [$user->id])
					->where('user_id', '<>', $user->id)
					->pluck('user_id');
				$User_invite = User_invite::whereIn('user_id', $invites_id)
					->selectRaw('sum(`uh_t`)as h,sum(`u_price`) as p')
					->first();
				$zh_t = User_invite::whereIn('user_id', $user->Teams->pluck('user_id'))
					->sum('uh_t');
				$u_invite = User_invite::firstOrNew(['user_id' => $user->id]);

				if ($u_invite->zh_t <> $zh_t) {
					$msg[] = [
						'直推数错误:' => [
							'id:'   => $u_invite->id,
							'u_id:' => $u_invite->user_id,
							'原:'    => $u_invite->zh_t,
							'应:'    => $zh_t,
						],
					];
				}
				if ($u_invite->allh_t <> ($User_invite->h ?? '0.00')) {
					$msg[] = [
						'总t数错误:' => [
							'id:'   => $u_invite->id,
							'u_id:' => $u_invite->user_id,
							'原:'    => $u_invite->allh_t,
							'应:'    => $User_invite->h ?? 0,
						],
					];
				}
				if ($u_invite->all_price <> ($User_invite->p ?? '0.00')) {
					$msg[] = [
						'总金额错误:' => [
							'id:'   => $u_invite->id,
							'u_id:' => $u_invite->user_id,
							'原:'    => $u_invite->all_price,
							'应:'    => $User_invite->p ?? 0,
						],
					];
				}
				if ($do) {
					$u_invite->zh_t = $zh_t;
					$u_invite->allh_t = $User_invite->h ?? 0;
					$u_invite->all_price = $User_invite->p ?? 0;
					$u_invite->save();
				}
			});
		return [
			$msg,
			$do ? '已修复' : '待核定',
		];
	}

	// 检查用户流水
	public static function check_finance_balance($do = 0, $coin = 'balance', $user_ids = [])
	{
		$precision = Finance::get_fin_pr($coin);

		$users = User::select('id', $coin);
		if ($user_ids) $users->wherein('id', $user_ids);

		return $users->get()
			->map(function ($user) use ($do, $coin, $precision) {
				$msg = [];
				//检查流水是否连续
				$fins = Finance::get_fin_class($coin)
					->select('user_balance', 'cash', 'id')
					->where('user_id', $user->id)
					->orderBy('time', 'asc')
					->orderBy('id', 'asc')// 当第一个条件相等时，才会用第二个条件去排序z
					->get();
				$balance = 0;
				$err_fin = $fins
					->map(function ($fin) use ($do, $precision, $fins, &$balance) {
						$balance = bcadd($balance, $fin->cash, $precision);
						if ($fin->user_balance <> $balance) {
							$msg = [
								'流水id:' => $fin->id,
								'记录余额:' => $fin->user_balance,
								'该有余额:' => $balance,
							];
							if ($do) {
								$fin->user_balance = $balance;
								$fin->save();
							}
							return $msg;
						}
					})
					->filter()// 去掉无效数据
					->values()
					->toArray();
				if ($err_fin)
					$msg = array_merge($msg, [
						'用户: ' . $user->id . ' 流水错误' => $err_fin,
					]);
				//检查余额和最后一笔流水是否匹配
				if (bccomp($user->$coin, $balance, 7)) {
					$msg = array_merge($msg,
						[
							'用户: ' . $user->id . ' 余额错误' => [
								'当前余额' => $user->$coin,
								'应有余额' => $balance,
							],
						]
					);
					if ($do) {
						$user->$coin = $balance;
						$user->save();
					}
					return $msg;
				}
			})
			->filter()// 去掉无效数据
			->values()
			->toArray();
	}

	// 测试发送邮件
	public static function push_sms_test()
	{
		$code = rand(1000, 9999);
		$Phone = '445388818@qq.com';
		$msg = '您的验证码是：' . $code . '，该验证码20分钟内有效，请勿泄漏于他人！test';

		dispatch(new EmailJob($msg, $Phone)); //列队发送

		//直接发送
//		Mail::raw($msg, function ($message) use ($Phone) {
//			$message->subject('[先东科技]验证邮件 ');
//			$message->to($Phone);
//		});


		return Common::re_msg(1, '验证邮件发送');
	}

	// 发放udb
	public static function push_udb()
	{
		$phs = [
			13992119976         => 25,
			13369278086         => 10,
			13909116126         => 25,
			18891525588         => 10,
			17343954134         => 70,
			18192358769         => 10,
			13165733658         => 75,
			15077596858         => 520,
			13907672398         => 10,
			'2239860295@qq.com' => 10,

		];

//		return collect(array_keys($phs))->diff(User::whereIn('phone', array_keys($phs))->pluck('phone'));  //不存在的用户

		// 写入udb
		return User::whereIn('phone', array_keys($phs))
			->get()
			->map(function ($item) use ($phs) {
				$item->UDB = $phs[$item->phone];
				$item->save();
			});
	}
}

