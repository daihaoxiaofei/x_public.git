<?php

namespace Tool;

use App\Http\Model\Finance_fil;
use App\Http\Model\Finance_fil_d;
use App\Http\Model\Goods;
use App\Http\Model\Order;
use App\Http\Model\Team;
use App\Http\Model\User;
use App\Http\Model\User_invite;
use Illuminate\Support\Str;

class Tool
{
	// 类说明
	// 用于添加用户 激活订单 平衡账户 添加和梳理流水

// return [
	// 注册账号
// Tool::register_user('2582496937@qq.com', 'sara'),
// Tool::register_user('18976465566', '强生'),
// Tool::register_user('18889761221', '棉布裙'),
// 添加订单
// Tool::activate_order('2582496937@qq.com', 100),
// Tool::activate_order('18976465566', 50),
// Tool::activate_order('18889761221', 100),
// 添加流水
// Tool::add_finance(Order::find(4145)),
// Tool::add_finance(Order::find(4144)),
// Tool::add_finance(Order::find(4143)),
// 平衡(弃用)
// Tool::balance_to_75300(),
//
// ];


	// 注册用户
	public static function register_user($phone, $nickname, $pwd = null, $re_id = 1)
	{
		//开启事物
		\DB::beginTransaction();

		$user = new User;
		$user->phone = $phone;
		$user->nickname = $nickname;
		$user->pwd = $pwd ?? Str::random(8);
		$user->time = time();
		$user->save();

		// 写入推荐表
		$inv_user = User::find($re_id);
		$teams = $user->id . ',' . $inv_user->Team->teams;

		$team = new Team();
		$team->user_id = $user->id;
		$team->recommend_id = $re_id;
		$team->teams = $teams;
		$team->save();

		\DB::commit();
		return $user;
	}

	// 添加并激活订单
	public static function activate_order($user, $number, $is_first = 1)
	{
		\DB::beginTransaction();
		if (!$user) return '用户不存在';
		$good = Goods::find(2);
		// 插入订单
		$order = new Order();
		$order->good_id = $good->id;
		$order->user_id = $user->id;
		$order->number = $number / $good->have_t;
		$order->price = $good->price * $order->number;
		$order->have_t = $number;
		$order->na = $user->nickname;
		$order->path = '自提';
		$order->phone = $user->phone;
		$order->time = time();

		// 直接激活
		$order->is_go = 1;
		$order->performer = 53;

		if ($is_first) {  // 生效时间
			$order->start_time = 1602777600;
		} else {
			$order->start_time = strtotime(date('Y-m-d')) + 24 * 3600;  //  第二天凌晨
		}
		$order->save();

		//记录购买者购买t 及 金额
		$u_inv = User_invite::firstOrNew(['user_id' => $order->User->id]);
		$u_inv->uh_t += $order->have_t;
		$u_inv->u_price += $order->price;
		$order->check_time = time();
		$u_inv->save();

		$Invite = $order->User->Invite;

		// 记录推荐人
		if ($Invite) {
			$u_invite = User_invite::firstOrNew(['user_id' => $Invite->id]);
			// 记录推荐人直推t数
			$u_invite->zh_t += $order->have_t;
			$u_invite->save();
			// 变更团队信息
			self::up_team($Invite, $order);
		}

		\DB::commit();
		return $order;
	}

	// 将账户总数平衡为 75300
	public static function balance_to_75300()
	{
		// 统计现有总数 多出的从某账号中排除
		// 需要 修改订单中t数 同比减少流水中金额 并整理流水
		// 修改统计数据
		$sum_t = Order::selectraw('(sum(have_t) + sum(give_t)) as all_t')
			->where('is_go', 1)
			->first()
			->all_t;

		$diff_t = $sum_t - 75300;
		if ($diff_t == 0) return '无需平衡';
		$order = Order::find(1247);// 用于平衡的订单

		// 修改流水 等比减少
		$tate = ($order->have_t - $diff_t) / $order->have_t;

		Finance_fil::where('user_id', $order->user_id)
			->whereIn('remarks', ['测试奖励', '每日产出', '账户盈余一次性释放', '每日释放'])
			->update([
				'cash' => \DB::raw('cash * ' . $tate),
			]);
		Finance_fil_d::where('user_id', $order->user_id)
			->whereIn('remarks', ['每日产出'])
			->update([
				'cash' => \DB::raw('cash * ' . $tate),
			]);
		Check::check_finance_balance(1, 'FIL', [$order->user_id]);
		Check::check_finance_balance(1, 'FIL_D', [$order->user_id]);

		// 修改订单
		$order->have_t -= $diff_t;
		$order->save();

		// 修改统计数据
		$user_invite = User_invite::find($order->user_id);
		$user_invite->uh_t = $order->have_t;
		$user_invite->save();

		return '平衡好了:' . -$diff_t;
	}

	// 修改订单T数
	public static function up_order_t($order, $new_t)
	{
		$sum_t = $order->have_t;

		// 修改流水 等比变动
		$tate = $new_t / $sum_t;

		$remarks = [
			'测试奖励',
			'每日产出',
			// '每日产出-分币一',
			// '每日产出-分币二',
			'每日释放',
			// '每日释放-分币一',
			// '每日释放-分币二',
			'账户盈余一次性释放',
		];
		Finance_fil::where('user_id', $order->user_id)
			->whereIn('remarks', $remarks)
			->update([
				'cash' => \DB::raw('cash * ' . $tate),
			]);

		Finance_fil_d::where('user_id', $order->user_id)
			->whereIn('remarks', $remarks)
			->update([
				'cash' => \DB::raw('cash * ' . $tate),
			]);
		Check::check_finance_balance(1, 'FIL', [$order->user_id]);
		Check::check_finance_balance(1, 'FIL_D', [$order->user_id]);

		// 修改订单
		$order->have_t = $new_t;
		$order->save();

		// 修改统计数据
		$user_invite = User_invite::find($order->user_id);
		$user_invite->uh_t = $order->have_t;
		$user_invite->save();

		return [
			'old'  => $sum_t,
			'new'  => $new_t,
			'tate' => $tate,
		];
	}

	// 添加流水并梳理
	public static function add_finance($order)
	{
		if (!$order) return '订单不存在';
		$user_id = 580; // 含有50T

		Finance_fil::where('user_id', $user_id)
			->whereIn('remarks', ['测试奖励', '每日产出', '每日释放', '账户盈余一次性释放'])
			->each(function ($old_fin) use ($order) {
				$fin = new Finance_fil;
				$fin->user_id = $order->user_id;
				$fin->cash = $old_fin->cash * $order->have_t / 50;  // 按比例添加
				$fin->remarks = $old_fin->remarks;
				$fin->time = $old_fin->time;
				$fin->save();
			});
		Finance_fil_d::where('user_id', $user_id)
			->whereIn('remarks', ['每日产出'])
			->each(function ($old_fin) use ($order) {
				$fin = new Finance_fil_d;
				$fin->user_id = $order->user_id;
				$fin->cash = $old_fin->cash * $order->have_t / 50;
				$fin->remarks = $old_fin->remarks;
				$fin->time = $old_fin->time;
				$fin->save();
			});

		Check::check_finance_balance(1, 'FIL', [$order->user_id]);
		Check::check_finance_balance(1, 'FIL_D', [$order->user_id]);

		return '添加好了';
	}

	// 变更所有推荐人的奖励和等级
	private static function up_team($Invite, $order)
	{
		$u_invite = User_invite::firstOrNew(['user_id' => $Invite->id]);

		$u_invite->all_price = bcadd($u_invite->all_price, $order->price, 3); //总金额
		$u_invite->allh_t = bcadd($u_invite->allh_t, $order->have_t, 0);//总t数
		$u_invite->save();
		if ($Invite->Invite) {
			self::up_team($Invite->Invite, $order);
		}
	}


}

